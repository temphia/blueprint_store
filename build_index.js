const fs = require("fs");
const zlib = require("zlib");
const path = require("path");

const root = "./data";
const index = [];

const files = fs.readdirSync(root);

files.forEach((group, rootIdx) => {
  const gfiles = fs.readdirSync(path.join(root, group));

  gfiles.forEach((bprint, gIdx) => {
    console.log("Processing =>", root, group, bprint);

    const buf = fs.readFileSync(path.join(root, group, bprint, "./index.json"));
    const bprintInfo = JSON.parse(buf);

    if (bprintInfo["slug"] !== bprint || bprintInfo["group"] !== group ) {
      throw new Error(
        `invalid name structure [${group} / ${bprint}] => [${bprintInfo["slug"]} / ${bprintInfo["group"]}]`
      );
    }

    index.push({
      name: bprintInfo["name"],
      slug: bprintInfo["slug"],
      group: bprintInfo["group"],
      subgroup: bprintInfo["subgroup"],
      description: bprintInfo["description"],
      icon: bprintInfo["icon"],
      tags: bprintInfo["tags"],
    });
  });
});

const jindex = JSON.stringify(index, null, 2);

fs.writeFileSync("./index.json", jindex);

const zip = zlib.gzip(jindex, (err, resp) => {
  fs.writeFileSync("./index.json.zip", resp, () => {});
});
